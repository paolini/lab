import sys, pygame, time
from background import Background
from ball import Ball

class Game(object):
    def __init__(self):
        self.size = (320, 240)
        self.background = Background(self.size)
        self.ball = Ball()

        pygame.init()

        self.screen = pygame.display.set_mode(self.size)

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    # documentazione: http://www.pygame.org/docs/ref/key.html
                    return
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    print "mouse key down"
                elif event.type == pygame.MOUSEBUTTONUP:
                    print "mouse key up"

            self.ball.update(self.screen)

            self.background.draw(self.screen)
            self.ball.draw(self.screen)

            time.sleep(0.01)

            pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run()

