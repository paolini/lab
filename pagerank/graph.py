import re, requests, random
prog = re.compile('href="([^"]*)"')      #regular expression --> cerco corrispondenze di espressione regolare


class Node(object):
    def __init__(self,link):
        self.link = link
        self.nodes = []         #lista di nodi in cui posso andare
    def add_arc(self,u):
        self.nodes.append(u)
    def __str__(self):
        return self.link
    def stampa(self):
        for line in self.nodes:
            print line
    def add_links(self,graph,depth):
        """
         carica la pagina self.sito e aggiunge tutti i link
         esegue sui nodi aggiunti ricorsivamente per recurse_level volte
        """
        r = requests.get(self.link)
        for line in r.content.split('/n'):
            matches = prog.findall(line)
            for m in matches:
                if 'http://' in m[0:7]:
                    site = m
                else:
                    if 'html' in m:
                        site = 'http://www.unifi.it/' + m
                    else:
                        site = None
                if site:
                    node = Node(site)
                    graph.add_node([node])
                    self.add_arc(node)
                    if depth>0:
                        node.add_links(graph,depth-1)


class Graph(object):
    def __init__(self):
        self.links = []
    def add_node(self,list):
        for l in list:
            self.links.append(l)
    def esplora(self,length):
        for node in self.links:
            node.count = 0
        pos = None
        l = []
        ll = length
        while length > 0:
            nodes = [random.choice(self.links)]
            if pos is not None:
                nodes += pos.nodes
            pos = random.choice(nodes)
            pos.count += 1
            length -= 1
        for node in self.links:
            if node.count>0: #non interessano i nodi non visitati, hanno rank 0
                l.append([node.count,node.link,float(node.count)/ll]) #lista con numero di visite del nodo, il nodo e il rank
                l.sort(reverse=True)
        print 'Visite   Rank         Sito'
        for line in l:
            print ' ',line[0],'   ',line[2],' ',line[1]
