"""
Progetto di Aiazzi Gabriele, Biagioli Giovanni, Vignolini Francesca
Base dell'algoritmo di pagerank, partendo dalla pagina web http://www.unifi.it
"""

from graph import Graph,Node
import random

g=Graph()
unifi = Node('http://www.unifi.it')
l = [unifi]
g.add_node(l)  #lista di tutti i siti raggiungibili
unifi.add_links(g,0)  #aggiunge tutti i siti a distanza 0

row= random.choice(g.links) #sceglie a caso un sito
print 'Il cammino parte da:' ,row, '\n'
length = raw_input('Indicare la lunghezza del cammino da compiere ') #l'utente inserisce la lunghezza del cammino
g.esplora(int(length)) #fa un cammino lungo length partendo da row