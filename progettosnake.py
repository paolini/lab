imm_sfondo = "immprogetto.jpg"       # importo imm di sfondo del gioco

import pygame                           # importo la libreria per il gioco

from pygame.locals import *

from sys import exit

pygame.init()

screen = pygame.display.set_mode((640,480), DOUBLEBUF | HWSURFACE, 32)

pygame.display.set_caption("Benvenuto in snake")

sfondo = pygame.image.load(imm_sfondo).convert()

while True:

    for event in pygame.event.get():

        if event.type == QUIT:

            exit()

    screen.blit(sfondo,(0,0))

    pygame.display.flip()

 


