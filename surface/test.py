import numpy as np
import math
from mesh import Mesh

#TODO: Calcolare i punti iniziali della curva

verticesOnCurve = [0,1,2,3]

def curva(p):
        x=math.cos(p*(2*math.pi))
        y=math.sin(p*(2*math.pi))
        z=math.sin(p*math.pi)
        return np.array([x, y, z])

vertices = np.array([list(curva(p)) + [p] for p in [0.0,0.25,0.5,0.75] ])
triangles = np.array([(0,1,2),(0,2,3)])

print vertices

mesh = Mesh(vertices,triangles,verticesOnCurve,curva)
mesh.triangulate()
mesh.triangulate()
mesh.triangulate()
mesh.show()
