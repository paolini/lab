import numpy as np

class Mesh(object):
    def __init__(self,vertices,triangles,verticesOnCurve,curva):
        self.vertices = vertices
        self.triangles = triangles
        self.curva = [curva] # non posso memorizzare una funzione in un attributo
        self.verticesOnCurve = verticesOnCurve
        #self.vertticesxx[:self.vertices.shape[0]] = self.vertices 
    
    def show(self):
        from mayavi import mlab
        x = self.vertices[:,0]
        y = self.vertices[:,1]
        z = self.vertices[:,2]
        mesh = mlab.triangular_mesh(x,y,z,self.triangles,
                                    representation='wireframe') 
        mlab.show()

    def triangulate(self):
        edges = {}    # Dizionario contenente gli spigoli (la chiave) e 
        # il punto medio (il valore)

        # metodo che crea il punto medio
        # di uno spigolo e salva nel dizionario edges

        def ordina(i,j):
            if i<j:
                return (i,j)
            else:
                return (j,i)

        def add_edge(i,j):
            i = int(i)
            j = int(j)
            if j < i:
                i,j = j,i
            if (i,j) not in edges:
                p = self.vertices[i]
                q = self.vertices[j]

                p_medio= 0.5 * (p+q)
                edges[(i,j)] = p_medio

                if (i in self.verticesOnCurve) and (j in self.verticesOnCurve):
                    # sono sul bordo
                    ii = self.verticesOnCurve.index(i)
                    jj = self.verticesOnCurve.index(j)
                    if jj<ii:
                        ii,jj = jj,ii
                    assert ii<jj
                    n = len(self.verticesOnCurve)
                    if jj==ii+1 or (ii==0 and jj==n-1):
                        # sono consecutivi
                        p_medio[0:3] = self.curva[0](p_medio[3])
                        if ii>0:
                            self.verticesOnCurve.insert(ii,(i,j)) # invece dell'indice metto gli estremi del segmento vedi (**)
                        else:
                            self.verticesOnCurve.append((i,j))

        # calcolo il punto medio di ciascuno spigolo di tutti i  triangoli 
        for t in self.triangles:
            add_edge(t[0],t[1])   # primo lato
            add_edge(t[1],t[2])   # secondo lato
            add_edge(t[2],t[0])   # terzo lato

        print (edges)
        n_edges = len(edges)                               # numero di punti medi
        n_triangles = self.triangles.shape[0]              # numero di triangoli presenti
        n_vertices = self.vertices.shape[0]                # numero di vertici
        new_vertices = np.zeros((n_vertices + n_edges, 4))  # i vrici totali saranno i precedenti piu' i punti medi

        new_vertices[:n_vertices] = self.vertices          # copia i vecchi vertici nella nuova struttura
        i = n_vertices
        for e in edges:                                    
            new_vertices[i] = edges[e]                     # inserisco l'iesimo punto medio nella posizione opportuna (i)
            if e in self.verticesOnCurve:
                self.verticesOnCurve[self.verticesOnCurve.index(e)] = i # (**) mette l'indice corretto
            edges[e] = i                                   # sostituisce alle coordinate del punto medio il suo numero
            i += 1
        #TODO: aggiorno i triangoli
        # ogni triangolo da' origine a 4 triangoli
        new_triangles = []
        for t in self.triangles:
            i,j,k = t
            
            ij = edges[ordina(i,j)]
            jk = edges[ordina(j,k)]
            ik = edges[ordina(i,k)]
            new_triangles.append((i,ij,ik))
            new_triangles.append((ik,k,jk))
            new_triangles.append((ik,ij,jk))
            new_triangles.append((ij,j,jk))
        self.triangles = np.array(new_triangles)
        self.vertices = new_vertices
