#!/usr/bin/env python
# -*- coding: utf-8 -*-

from players import *

"""
File delle impostazioni di gioco

FORMATO

Le impostazioni di gioco devono essere contenute in un dizionario con chiavi

{
	"white": ...
	"black": ...
}

A ciascuna chiave deve essere associata un dizionario del tipo

{
	"type": ...
	"params": (facoltativa)
}

La chiave "type" descrive il tipo del giocatore.
Il tipo deve essere tra quelli implementati nel file "players.py".
I tipi al momento supportati sono HumanPlayer, AIPlayer.

La chiave "params" fornisce eventuali parametri aggiuntivi.
Ad essa deve essere associata un dizionario "nome_parametro": valore.

DETTAGLI

Il tipo HumanPlayer non supporta alcun parametro aggiuntivo.

Il tipo AIPlayer supporta i seguenti parametri:

	"mad"		(float tra 0 ed 1; default = 0)		Propensità del giocatore AI ad effettuare mosse casuali
	"risky"		(float tra 0 ed 1; default = 0)		Propensità del giocatore AI ad effettuare mosse azzardate
	"alphabeta"	(float tra 0 ed 1; default = 0.6)	Propensità del giocatore AI a ignorare mosse poco interessanti
	"MAXDEPTH" 	(int > 0; default = 2)			Massima profondità dell'albero decisionale
								Per MAXDEPTH = 3 si raccomandano 2 GB di RAM e CPU Dual Core 3.0GHz o superiori
"""

CONFIGS = \
{ "white": 
	{ "type": HumanPlayer }, 
  "black": 
  	{ "type": AIPlayer, 
  	  "params": 
  	  	{ "mad" : 0.0,
  	  	  "risky": 0.0,
  	  	  "alphabeta": 0.6,
  	  	  "MAXDEPTH": 2 }
  	 }
 }
