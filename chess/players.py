#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Modulo che definisce la classe Player per un giocatore astratto,
		e diverse classi figle con implementazioni specifiche """

from pieces import *
import board, intelligence
import threading, pygame, time

class Game(object):
	""" Classe che arbitra sulla partita, decidendo a chi spetta la prossima mossa
	board = la scacchiera di gioco
	white, black = giocatore bianco/nero risp. 
	history = una lista di coppie (Position, Position) dei turni precedenti"""

	def __init__(self, disp, params):
		self.board = board.BoardGUI(disp, "img/pieces.png")
		
		whiteconf = params["white"]
		blackconf = params["black"]
		
		if "params" not in whiteconf:
			whiteconf["params"] = { }
			
		if "params" not in blackconf:
			blackconf["params"] = { }
		
		self.white = whiteconf["type"](self.board, "white", whiteconf["params"])
		self.black = blackconf["type"](self.board, "black", blackconf["params"])
		
		self.white.setOpponent(self.black)
		self.history = []
	
	def playTurn(self):
		""" Restituisce una coppia (Position, Position) se il giocatore
			a cui spetta il turno ha deciso una mossa, None se non ha ancora deciso """

		m = self.whoseTurn().chosenMove
		self.whoseTurn().chosenMove = None

		return m

		
	def whoseTurn(self):
		""" Restituisce il giocatore a cui spetta il turno """
		if len(self.history) % 2 == 0: # I turni pari sono del bianco
			return self.white
		else:
			return self.black


class Player(object):
	""" Classe che definisce un giocatore astratto 
	board = la scacchiera di gioco
	chosenMove = None se il giocatore deve ancora fare una mossa, altrimenti una coppia (Position, Position)
	ponderingMove = la posizione da spostare su cui il giocatore sta riflettendo
	team = black oppure white 
	opponent = l'oggetto Player avversario """

	
	def __init__(self, board, team, params):
		self.board = board
		self.team = team
		
		self.chosenMove = None
		self.ponderingMove = None

		self.opponent = None
		
		# Crea un immagine vuota, da usare nel caso la sottoclasse non ne utilizzi una propria
		loseImage = pygame.Surface((0, 0))
	
	def setOpponent(self, player):
		""" Imposta un avversario """
		
		self.opponent = player
		player.opponent = self
	
	def userSelected(self, x, y):
		""" Efficace solo se è il turno di un giocatore umano. Implementato in override """
		pass

	def userUnselected(self):
		""" Efficace solo se è il turno di un giocatore umano. Implementato in override """
		pass
		
	def lose(self, disp):
		""" Stampa un messaggio di vittoria/sconfitta sul display di pygame disp """
		
		# Cerca di centrare il messaggio sullo schermo
		x, y = disp.get_size()
		dx, dy = self.loseImage.get_size()
		
		x = (x - dx)/2
		y = (y - dy)/2
		
		disp.blit(self.loseImage, (x, y))
	
	def reportMove(self, pos, newpos):
		""" Efficace solo se è il turno di un giocatore AI. Implementato in override """
		pass


class HumanPlayer(Player):
	""" Definisce un giocatore umano, controllato tramite l'interfaccia grafica """

	def __init__(self, board, team, params):
		super(HumanPlayer, self).__init__(board, team, params)
		
		self.loseImage = pygame.image.load("img/youlose.png").convert_alpha()
				
	def userSelected(self, x, y):
		""" Comunica al giocatore attuale che l'utente ha scelto
		un pezzo tramite l'interfaccia grafica. Efficace solo se è il turno di un giocatore umano. """
				
		newpos = self.board.getPosition(x, y)
		
		# Se il giocatore aveva già scelto con un clic una posizione, si verifica che la mossa sia legale ...
		if newpos in self.board.highlight and self.board.isLegal(self.ponderingMove, newpos):
			self.chosenMove = (self.ponderingMove, newpos)		# ... in tal caso sarà eseguita il prima possibile
		elif newpos.p is not None and newpos.p.team == self.team:	# Altrimenti, il giocatore sta scegliendo un pezzo da muovere
			self.ponderingMove = newpos
			self.board.highlight = newpos.p.enumMoves(self.board)	# Evidenzio le posizioni possibili per quel pezzo
			
			for pos in self.board.highlight:
				if not self.board.isLegal(newpos, pos):
					self.board.highlight.remove(pos)	# Elimino le mosse illegali

	def userUnselected(self):
		""" Comunica che l'utente vuole annullare la mossa in corso """
		
		self.chosenMove = None
		self.ponderingMove = None
		self.board.highlight = []


class AIRun(threading.Thread):
	def __init__(self, player):
		super(AIRun, self).__init__()

		self.player = player

	def run(self):
		self.player.chosenMove = self.player.simTree.selectMove()

	
class AIPlayer(Player):
	""" Definisce un giocatore controllato dall'intelligenza artificiale """

	def __init__(self, board, team, params):
		super(AIPlayer, self).__init__(board, team, params)

		self.lastMove = None
		self.aithread = AIRun(self)

		try:
			self.simTree = intelligence.SimulationTree(self, params)
		except ChessError, e:
			raise ChessError("Impossibile caricare il file config.py\nSaranno caricati i valori predefiniti\n" + str(e))
		
		self.loseImage = pygame.image.load("img/youwin.png").convert_alpha()

	def reportMove(self, pos, newpos):
		""" Comunica al giocatore la mossa avversaria, e di iniziare a riflettere sul proprio turno """

		self.simTree.opponentMove(pos, newpos)
		
		if not self.board.isStuck(self.team):

			if self.aithread.isAlive():
				self.aithread.join()

			self.aithread = AIRun(self)
			self.aithread.start()

		
# Parametri di gioco di default, nel caso l'importazione del file config.py fallisse

DEFAULTS = \
{ "white": 
	{ "type": HumanPlayer }, 
  "black": 
  	{ "type": AIPlayer, 
  	  "params": 
  	  	{ "mad" : 0.0,
  	  	  "risky": 0.0,
  	  	  "MAXDEPTH": 2 }
  	 }
 }

