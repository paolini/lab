# Programma di scacchi per il Laboratorio Software 2013/14 #
CHESS: Chess for Humans, Engines and Similar Software

Cose da fare:
- Regole per en-passant, promozione, e arrocco
- Regole per stallo da ripetizione
- Multithreading

Stretch goals:
- Un menù per selezionare il tipo di avversario, la difficoltà, eccetera.
- Una "modalità debug" che mostra informazioni sul programma , eccetera
- Multiplayer online/LAN

# PER TESTARE #

Prima di avviare, aprire il file "config.py" con un editor di testo per modificare le impostazioni

$ python main.py

