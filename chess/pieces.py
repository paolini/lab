#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Modulo che definisce una rappresentazione della scacchiera, dei pezzi,
					 e delle loro regole di movimento """

import json, random

class Position(object):
	""" Definisce una posizione della scacchiera
	x = posizione orizzontale (0, 1, 2,... 7)
	y = posizione verticale (0, 1, 2,... 7)
	p = il pezzo che occupa la posizione (eventualmente None) """
	
	x = None
	y = None
	p = None
	
	def __init__(self, j):
		""" Costruisce una posizione deserializzando l'oggetto JSON """
		
		x = j['x']
		y = j['y']
		if 'p' in j.keys() and j['p'] is not None:
			pj = j['p']
			p = PIECESET[pj['ID']](pj)
		else:
			p = None
			
		if not (x in range(8) and y in range(8)):
			raise ValueError
		else:
			self.x = x
			self.y = y
		
		if p is not None:
			p.setPos(self)
			
		self.p = p
		
	def __eq__(self, pos):
		return self.x==pos.x and self.y==pos.y
		
	def __repr__(self):
		return "abcdefgh"[self.x]+"87654321"[self.y]


class Piece(object):
	""" Definisce un pezzo astratto 
	team = 'black' oppure 'white'
	hasMoved = True se il pezzo si è spostato dalla posizione iniziale
	pos = posizione attuale """
	
	team = None
	hasMoved = False
	
	pieceId = None
	
	def __init__(self, j):
		""" Costruisce una pezzo caricando l'oggetto JSON j """
		
		self.pos = None
		self.team = j['team']
		self.hasMoved = j['hasMoved']
	
	def setPos(self, pos):
		self.pos = pos
		pos.p = self
		
	def enumMoves(self, board):
		""" Enumera le mosse pseudolegali per il pezzo
		Metodo da implementare in override """
		
		return []
	
	def enumAllDelta(self, board, dx, dy):
		""" Enumera tutte le posizioni pseudolegali nella direzione dx, dy """
		moves = []
		x = self.pos.x + dx
		y = self.pos.y + dy

		while x in range(8) and y in range(8):
			newpos = board.getPosition(x, y)
			# Si ferma se incontra un ostacolo, alleato oppure nemico
			if newpos.p is not None:
				if self.team != newpos.p.team:
					moves += [newpos]
				break
			moves += [newpos]
			x += dx
			y += dy
		return moves
		
		
class Pawn(Piece):
	pieceId = 5
	
	def enumMoves(self, board):
		""" Enumera le possibili mosse per il pedone """
		moves = []
		
		x = self.pos.x
		y = self.pos.y
		
		if self.team == "white":
			dy = -1
		else:
			dy = 1
		
		if y + dy in range(8):
			# Il pedone può sempre muoversi verso le fila avversarie, ma senza attaccare
			newpos = board.getPosition(x, y + dy)
			if newpos.p is None:
				moves += [newpos]
			# Il pedone può muoversi due caselle in avanti se non si è ancora mosso e c'è spazio
			if not self.hasMoved and board.getPosition(x, y + dy).p is None:
				newpos = board.getPosition(x, y + 2*dy)
				if newpos.p is None:
					moves += [newpos]
			# Il pedone può sempre attaccare in diagonale
			for dx in [-1, 1]:
				if x + dx in range(8):
					newpos = board.getPosition(x + dx, y + dy)
					if newpos.p is None or self.team == newpos.p.team:
						continue
					moves += [newpos]
		
		return moves
	# TODO manca l'en passant
	# TODO manca la promozione
	
class Rook(Piece):
	pieceId = 2

	def enumMoves(self, board):
		""" Enumera le possibili mosse per il castello """
		return self.enumAllDelta(board, 1, 0) + self.enumAllDelta(board, -1, 0) +\
			self.enumAllDelta(board, 0, 1) + self.enumAllDelta(board, 0, -1)
		# TODO manca l'arrocco

class Bishop(Piece):
	pieceId = 3
		
	def enumMoves(self, board):
		""" Enumera le possibili mosse per l'alfiere """
		return self.enumAllDelta(board, 1, 1) + self.enumAllDelta(board, -1, 1) +\
			self.enumAllDelta(board, 1, -1) + self.enumAllDelta(board, -1, -1)
	
class Knight(Piece):
	pieceId = 4
	
	def enumMoves(self, board):
		""" Enumera le possibili mosse per il cavallo """
		moves = []

		for dx in [-1, 1]:
			for dy in [-2, 2]:
				x = self.pos.x + dx
				y = self.pos.y + dy
				if x in range(8) and y in range(8):
					newpos = board.getPosition(x, y)
					if newpos not in moves:
						moves += [newpos]
		for dx in [-2, 2]:
			for dy in [-1, 1]:
				x = self.pos.x + dx
				y = self.pos.y + dy
				if x in range(8) and y in range(8):
					newpos = board.getPosition(x, y)
					if newpos not in moves:
						moves += [newpos]

		return moves
	
class Queen(Piece):
	pieceId = 1
	
	def enumMoves(self, board):
		""" Enumera le possibili mosse per la regina """
		return self.enumAllDelta(board, 1, 0) + self.enumAllDelta(board, -1, 0) +\
			self.enumAllDelta(board, 0, 1) + self.enumAllDelta(board, 0, -1) +\
			self.enumAllDelta(board, 1, 1) + self.enumAllDelta(board, -1, 1) +\
			self.enumAllDelta(board, 1, -1) + self.enumAllDelta(board, -1, -1)

class King(Piece):
	pieceId = 0
	
	def enumMoves(self, board):
		""" Enumera le possibili mosse per il re """
		moves = []

		for dx in [-1, 0, 1]:
			for dy in [-1, 0, 1]:
				if dx == 0 and dy == 0:		# Il re non può restare fermo
					continue
				x = self.pos.x + dx
				y = self.pos.y + dy
				if x in range(8) and y in range(8):
					newpos = board.getPosition(x, y)
					moves += [newpos]
		
		return moves
		# TODO manca l'arrocco

# Costanti utili

PLAYERNAMES = ["white", "black"]

# Un dizionario pieceId : tipo
	
PIECESET = {}
for pc in [King, Queen, Rook, Knight, Bishop, Pawn]:
	PIECESET[pc.pieceId] = pc
	
# Definisce lo stato iniziale di una scacchiera

INITIALSTATE = """{"BoardMatrix": [[{"y": 0, "x": 0, "p": {"hasMoved": false, "ID": 2, "team": "black"}}, {"y": 1, "x": 0, "p": {"hasMoved": false, "ID": 5, "team": "black"}}, {"y": 2, "x": 0}, {"y": 3, "x": 0}, {"y": 4, "x": 0}, {"y": 5, "x": 0}, {"y": 6, "x": 0, "p": {"hasMoved": false, "ID": 5, "team": "white"}}, {"y": 7, "x": 0, "p": {"hasMoved": false, "ID": 2, "team": "white"}}], [{"y": 0, "x": 1, "p": {"hasMoved": false, "ID": 4, "team": "black"}}, {"y": 1, "x": 1, "p": {"hasMoved": false, "ID": 5, "team": "black"}}, {"y": 2, "x": 1}, {"y": 3, "x": 1}, {"y": 4, "x": 1}, {"y": 5, "x": 1}, {"y": 6, "x": 1, "p": {"hasMoved": false, "ID": 5, "team": "white"}}, {"y": 7, "x": 1, "p": {"hasMoved": false, "ID": 4, "team": "white"}}], [{"y": 0, "x": 2, "p": {"hasMoved": false, "ID": 3, "team": "black"}}, {"y": 1, "x": 2, "p": {"hasMoved": false, "ID": 5, "team": "black"}}, {"y": 2, "x": 2}, {"y": 3, "x": 2}, {"y": 4, "x": 2}, {"y": 5, "x": 2}, {"y": 6, "x": 2, "p": {"hasMoved": false, "ID": 5, "team": "white"}}, {"y": 7, "x": 2, "p": {"hasMoved": false, "ID": 3, "team": "white"}}], [{"y": 0, "x": 3, "p": {"hasMoved": false, "ID": 1, "team": "black"}}, {"y": 1, "x": 3, "p": {"hasMoved": false, "ID": 5, "team": "black"}}, {"y": 2, "x": 3}, {"y": 3, "x": 3}, {"y": 4, "x": 3}, {"y": 5, "x": 3}, {"y": 6, "x": 3, "p": {"hasMoved": false, "ID": 5, "team": "white"}}, {"y": 7, "x": 3, "p": {"hasMoved": false, "ID": 1, "team": "white"}}], [{"y": 0, "x": 4, "p": {"hasMoved": false, "ID": 0, "team": "black"}}, {"y": 1, "x": 4, "p": {"hasMoved": false, "ID": 5, "team": "black"}}, {"y": 2, "x": 4}, {"y": 3, "x": 4}, {"y": 4, "x": 4}, {"y": 5, "x": 4}, {"y": 6, "x": 4, "p": {"hasMoved": false, "ID": 5, "team": "white"}}, {"y": 7, "x": 4, "p": {"hasMoved": false, "ID": 0, "team": "white"}}], [{"y": 0, "x": 5, "p": {"hasMoved": false, "ID": 3, "team": "black"}}, {"y": 1, "x": 5, "p": {"hasMoved": false, "ID": 5, "team": "black"}}, {"y": 2, "x": 5}, {"y": 3, "x": 5}, {"y": 4, "x": 5}, {"y": 5, "x": 5}, {"y": 6, "x": 5, "p": {"hasMoved": false, "ID": 5, "team": "white"}}, {"y": 7, "x": 5, "p": {"hasMoved": false, "ID": 3, "team": "white"}}], [{"y": 0, "x": 6, "p": {"hasMoved": false, "ID": 4, "team": "black"}}, {"y": 1, "x": 6, "p": {"hasMoved": false, "ID": 5, "team": "black"}}, {"y": 2, "x": 6}, {"y": 3, "x": 6}, {"y": 4, "x": 6}, {"y": 5, "x": 6}, {"y": 6, "x": 6, "p": {"hasMoved": false, "ID": 5, "team": "white"}}, {"y": 7, "x": 6, "p": {"hasMoved": false, "ID": 4, "team": "white"}}], [{"y": 0, "x": 7, "p": {"hasMoved": false, "ID": 2, "team": "black"}}, {"y": 1, "x": 7, "p": {"hasMoved": false, "ID": 5, "team": "black"}}, {"y": 2, "x": 7}, {"y": 3, "x": 7}, {"y": 4, "x": 7}, {"y": 5, "x": 7}, {"y": 6, "x": 7, "p": {"hasMoved": false, "ID": 5, "team": "white"}}, {"y": 7, "x": 7, "p": {"hasMoved": false, "ID": 2, "team": "white"}}]]}"""

INITIALSTATE = json.loads(INITIALSTATE)
			
class ChessError(Exception):
	""" Definisce un errore che può essere messo sotto controllo dal programma """
	pass
