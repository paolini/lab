#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Costruisce l'albero decisionale per l'intelligenza artificiale """

INF = 999999

from pieces import *
import board
import random, threading

class NodeRun(threading.Thread):
	def __init__(self, node):
		super(NodeRun, self).__init__()

		self.node = node

	def run(self):
		self.node.generateChildren()

class SimulationNode(object):
	""" Classe che definisce un nodo nell'albero decisionale
	board = la scacchiera che rappresenta
	lastMove = tupla (pos, newpos) dell'ultima mossa giocata
	final = True se la scacchiera è una posizione finale (stallo o scacco matto)
	children = figli del nodo
	parent = genitore del nodo
	tree = albero di appartenenza
	valuePairs = tupla (peggiore, migliore) dei valori attesi dalla mossa
	TURN = altezza assoluta del nodo (risp. alla posizione iniziale)
	"""

	def __init__(self, parent, pos, newpos):
		self.children = []
		self.parent = parent
		
		self.lastMove = (pos, newpos)
		
		self.tree = parent.tree
		self.TURN = parent.TURN + 1
		
		self.final = False
		
		self.board = parent.board.simulateMove(pos, newpos)
		self.valuePairs = (INF, -INF)
		
		self.generateChildren()
		self.evaluate()
	
	def __repr__(self):
		""" Rappresenta un nodo e tutti i suoi figli in formato leggibile """
		
		out = ""
		delta = self.TURN - self.tree.root.TURN
		
		out += "(" + str(delta) + ")" + ("\t" * delta)
		out += repr(self.lastMove[0]) + "->" + repr(self.lastMove[1]) + str(self.valuePairs) + "\n"
		
		for c in self.children:
			out += repr(c)
		
		return out
		
	def evaluate(self):
		""" Ritorna la tupla (punteggio peggiore, punteggio migliore) tra tutti i figli
		Se non ci sono figli, valuta il punteggio della scacchiera in base ai parametri di Shannon (1954)
			(Vengono aggiunti punti per ognuno dei propri pezzi,
				e sottratti per ogni pezzo avversario sulla scacchiera) """
		
		if self.children == []:
			
			team = PLAYERNAMES[self.TURN % 2]
			
			# Se non ci sono figli, la partita potrebbe essere finita in questo stato
			if self.board.isStuck(team):
				self.final = True
				
				if self.board.isCheckmate(team):
					if team == self.tree.player.team:
						self.valuePairs = (-INF, -INF)
					else:
						self.valuePairs = (INF, INF)
				else:
					# Altrimenti è stallo, ovvero pareggio
					self.valuePairs = (0, 0)
					
			# Altrimenti, calcola il punteggio di Shannon
			else:
				scores = {King:200, Queen:9, Rook:5, Knight:3, Bishop:3, Pawn:1}
				myscore = 0

				for pos in self.board:
					if pos.p is None:
						continue
					elif self.tree.player.team == pos.p.team:
						c = 1
					else:
						c = -1

					myscore += c*scores[type(pos.p)]

				self.valuePairs = (myscore, myscore)
		
		elif self.tree.player.team == PLAYERNAMES[self.TURN % 2]:
			for child in self.children:
				child.evaluate()
				self.valuePairs = (min(child.valuePairs[0], self.valuePairs[0]), max(child.valuePairs[1], self.valuePairs[1]))
				
			# Scarto i risultati poco interessanti
			# L'intervallo di interesse è "mossa scelta" +- "alphabeta * numero di mosse"
			# Tuttavia cerco di centrare l'intervallo in maniera da considerare l'alphabeta% delle mosse
			
			l = len(self.children)
			k = (l - 1) * (1 - self.tree.mad * random.random())
			k = int(k)
			
			# Se la strategia è risky = 0, le tuple vengono ordinate in base al primo elemento, ovvero in base peggior caso
			# In caso di uguaglianza, soltanto in base al peggior caso, e in caso di nuova uguaglianza, in base al migliore		
			self.children = sorted(self.children, key = \
					lambda node: (node.valuePairs[0] * (1-self.tree.risky) + node.valuePairs[1] * self.tree.risky, node.valuePairs) )
			
			a = k - self.tree.alphabeta * l / 2
			b = k + self.tree.alphabeta * l / 2 - min(a, 0)
			a = a - max(b - l, 0)
			a = int(max(a, 0))
			b = int(min(b, l))
			
			self.children = self.children[a:b]
		
	def generateChildren(self):
		""" Genera tutti i figli del nodo """
			
		# Se si è raggiunta la massima altezza, non si esegue l'operazione
		# Inoltre, non è mai necessario generare i figli di una posizione finale
		if self.TURN - self.tree.root.TURN < self.tree.MAXDEPTH and not self.final:
			
			# La procedura viene chiamata più volte, ma non è mai necessario ripeterla
			if self.children == []:
				for (pos, newpos) in self.board.enumAllMoves(PLAYERNAMES[self.TURN % 2]):
					self.children += [SimulationNode(self, pos, newpos)]
			else:
				threads = []
				for c in self.children:
					t = NodeRun(c)
					t.start()
					threads += [t]
				
				for t in threads:
					t.join()
				
		
		self.evaluate()

class RootNode(SimulationNode):
	""" Classe speciale per la radice dell'albero. Rappresenta la scacchiera iniziale """
	
	def __init__(self, tree):
		self.children = []
		self.parent = None
		
		self.lastMove = None
		
		self.tree = tree
		self.TURN = 0
		self.final = False
		
		self.board = board.Board()
		self.valuePairs = (-INF, -INF)
		
	def __repr__(self):
		out = "(0) INIZIO\n"
		
		for c in self.children:
			out += repr(c)
	
		
class SimulationTree(object):
	""" Classe che definisce l'albero decisionale delle mosse
	player = il giocatore di cui simulo le mosse
	game = la partita che sto simulando
	mad = propensità ad eseguire mosse casuali (float tra 0 ed 1)
	risky = propensità ad eseguire mosse azzardate (float tra 0 ed 1)
	alphabeta = propensità a scartare mosse poco interessanti (float tra 0 ed 1)
	root = radice dell'albero decisionale
	dict = dizionario delle posizioni già calcolate
	"""

	# mad = 0 vuol dire che la strategia è completamente determinata dalle mosse avversarie
	# mad = 1 vuol dire che la strategia è completamente arbitraria
	mad = 0.0
	
	# risky = 0 vuol dire che la strategia è minimax
	# risky = 1 vuol dire che la strategia è maximax
	risky = 0.0
	
	# alphabeta = 0 vuol dire che tutte le mosse non interessanti vengono rimosse dall'albero
	# alphabeta = 1 vuol dire che tutte le mosse nell'albero vengono conservate
	alphabeta = 0.6
	
	# Massima profondità dell'albero decisionale
	MAXDEPTH = 3
	
	def __init__(self, player, params):
		self.player = player
		
		if "mad" in params:
			self.mad = float(params["mad"])
			assert self.mad >= 0
			assert self.mad <= 1
			
		if "risky" in params:
			self.risky = float(params["risky"])
			assert self.risky >= 0
			assert self.risky <= 1
			
		if "alphabeta" in params:
			self.alphabeta = float(params["alphabeta"])
			assert self.alphabeta >= 0
			assert self.alphabeta <= 1
			
		if "MAXDEPTH" in params:
			self.MAXDEPTH = int(params["MAXDEPTH"])
			assert self.MAXDEPTH >= 0

		self.root = RootNode(self)
		self.curthread = NodeRun(self.root)
		
		self.root.generateChildren()
		
	def __repr__(self):
		""" Rappresenta l'albero decisionale in formato leggibile """
		
		return repr(self.root)

	def opponentMove(self, pos, newpos):
		foundMove = False
		
		for i in self.root.children:
			if i.lastMove == (pos, newpos):
				self.root = i
				foundMove = True
				break
		
		# L'avversario ha eseguito una mossa imprevista
		if not foundMove:
			self.root = SimulationNode(self.root, pos, newpos)

		if self.curthread.isAlive():
			self.curthread.join()

		self.curthread = NodeRun(self.root)
		self.curthread.start()
		

	def selectMove(self):
		r = self.root

		if self.curthread.isAlive():
			self.curthread.join()
		
		# Se la strategia è mad = 0, allora sceglie l'elemento len-esimo, cioè il massimo
		k = (len(r.children) - 1) * (1 - self.mad * random.random())
		k = int(k)
		
		if r.children == []:
			return None	# Evita crash dovuti all'assenza di nodi figli
					# Questo dovrebbe avvenire solo se MAXDEPTH = 0 oppure la posizione è finale
							
		self.root = r.children[k]
		
		self.root.generateChildren()
		
		return self.root.lastMove
	

