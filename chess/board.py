#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Modulo che definisce la rappresentazione grafica della scacchiera e dei pezzi """

import pygame, json
import players
from pieces import *

class ChessEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, Board):
			return {'BoardMatrix':obj.BoardMatrix}
		elif isinstance(obj, Position):
			return obj.__dict__
		elif isinstance(obj, Piece):
			return {'team':obj.team, "hasMoved":obj.hasMoved, 'ID':obj.pieceId}
		else:
			return json.JSONEncoder.default(self, obj)

class Board(object):
	""" Definisce una scacchiera come matrice di posizioni"""
	
	def __init__(self, j = INITIALSTATE):
		""" Costruisce la scacchiera da dati JSON 
		Se non ne vengono forniti, viene caricato lo stato iniziale """
		
		self.BoardMatrix = [[[None] for i in range(8)] for h in range(8)]
		
		bm = j['BoardMatrix']
		
		for x in range(8):
			for y in range(8):
				self.BoardMatrix[x][y] = Position(bm[x][y])
	
	def __iter__(self):
		""" Un ciclo for x in Board restituisce tutte le posizioni """
		
		for x in range(8):
			for y in range(8):
				yield self.getPosition(x, y)
				
	def __repr__(self):
		""" Rappresenta la scacchiera in formato leggibile """
		
		pieces = ["K","q" ,"r" ,"k" ,"b" ,"p"]
		res = ""
		line = ""
		
		for y in range(8):
			for x in range(8):
				pos = self.getPosition(x, y)
				p = pos.p
				
				res += str(pos) + "\t"
				
				if p is not None:
					line += p.team[0] + pieces[p.pieceId]
				line += "\t"
			
			res += "\n" + line + "\n\n"
			line = ""
		
		return res
	
	def getPosition(self, *args):
		""" Restituisce la casella richiesta in uno di due modi
		x, y = coordinate della casella 
		pos = oggetto posizione"""
		
		try:
			if len(args) == 1:
				pos = args[0]
				x, y = pos.x, pos.y
			elif len(args) == 2:
				x, y = args[0], args[1]
			
			return self.BoardMatrix[x][y]
		except IndexError:
			raise ValueError, "[Board] Si è tentato di accedere alla posizione non valida: %s"%str(args)

	def isLegal(self, pos, newpos):
		""" Verifica alcune condizioni base per uno spostamento.
		In particolare verifica che non siano attaccati alleati,
		e che la mossa non causi uno scacco per il gioc. del turno 
		pos = posizione del pezzo da spostare
		newpos = nuova posizione"""
		
		# Condizioni impreviste
		if newpos is None:
			return False
		if pos.p is None:
			return False
		
		team = pos.p.team
		
		# È illegale attaccare pezzi alleati (inclusi se stessi)
		if newpos.p is not None and team == newpos.p.team:
			return False
		
		# Sono illegali le mosse che mettono il proprio re in scacco
		
		return not self.simulateMove(pos, newpos).isCheck(team)
		
	def isCheck(self, team):
		""" Verifica se il giocatore team è in scacco """
		
		k = self.findKing(team)
		
		# Verifico se qualche pezzo nemico può attaccare il re
		for pos in self:
			p = pos.p
			if p is None:
				continue
			elif p.team != k.team and k.pos in p.enumMoves(self):
				return True
		return False
		
	def isStuck(self, team):
		""" Verifica se il giocatore team è privo di mosse valide """
		
		for pos in self:
			if pos.p is not None and pos.p.team == team:
				moves = pos.p.enumMoves(self)
				
				for newpos in moves:
					if self.isLegal(pos, newpos):	# c'è una mossa valida
						return False		# posso uscire dal ciclo
		return True
		
	def isCheckmate(self, team):
		""" Verifica se il giocatore team è in scacco matto """
		
		# team è in scacco matto se è in scacco e non ha mosse valide
		return self.isCheck(team) and self.isStuck(team)
		
	def isStalemate(self, team):
		""" Verifica se il giocatore team è in stallo """
		
		# team è in stallo se non è in scacco, ma non ha mosse valide
		return not self.isCheck(team) and self.isStuck(team)
			
	def findKing(self, team):
		""" Trova il re della squadra team """
		
		for pos in self:
			p = pos.p
			if type(p) is King and p.team == team:
				return p
	
	def move(self, pos, newpos):
		""" Esegue una mossa """
		try:
			pos, newpos = self.getPosition(pos), self.getPosition(newpos)
			
			pos.p.hasMoved = True
			
			# Promozione del pedone a regina
			if type(pos.p) is Pawn and newpos.y in [0, 7]:
				j = json.dumps(pos.p, cls = ChessEncoder)
				pos.p = Queen(json.loads(j))
			
			pos.p.setPos(newpos)
			pos.p = None
		except:
			raise ValueError, "[Board] Si è tentato di eseguire una mossa che ha causato un errore nel programma"
			
	def simulateMove(self, pos, newpos):
		""" Crea una copia della scacchiera su cui simulare le mosse successive """

		boardstring = json.dumps(self, cls = ChessEncoder)
		newboard = Board(json.loads(boardstring))
		
		newboard.move(pos, newpos)
		return newboard
		
	def enumAllMoves(self, team):
		for pos in self:
			if pos.p is not None and pos.p.team == team:
				for newpos in pos.p.enumMoves(self):
					if self.isLegal(pos, newpos):
						yield (pos, newpos)


class BoardGUI(Board):
	""" Estensione della scacchiera con rappresentazione grafica 
	disp = display pygame
	whiteSquare, blackSquare, highlSquare = colori
	piecesImage = array delle immagini relative a ciascun pezzo
	highlight = array delle caselle da evidenziare (es. mosse valide)"""
	
	disp = None
	
	# Definisce dei colori nel formato utilizzato da pygame
	whiteSquare = (210, 180, 140)
	blackSquare = (145, 129, 81)
	
	piecesImage = []
	
	highlight = []
	
	highlSquare = (0, 255, 0, 60)
	
	def __init__(self, disp, tilePath):
		super(BoardGUI, self).__init__()
		self.disp = disp
		self.highlight = []
		
		# Carica le immagini dei pezzi dal file dato
		image = pygame.image.load(tilePath).convert_alpha()
		imageWidth, imageHeight = image.get_size()
		imageWidth, imageHeight = int(round(imageWidth/6.0)), int(round(imageHeight/2.0))
		for x in range(0, 6):
			line = []
			for y in range(0, 2):
				rect = (x*imageWidth, y*imageHeight, imageWidth-1, imageHeight-1)
				line += [pygame.transform.scale(image.subsurface(rect), (80, 80))]
			self.piecesImage += [line]

	def draw(self):
		""" Rappresenta la scacchiera sul display """
		for pos in self:
			x, y = pos.x, pos.y
			r = pygame.Rect((x*80, y*80), (80, 80))
			if (x + y) % 2 == 0:
				pygame.draw.rect(self.disp, self.whiteSquare, r, 0)
			else:
				pygame.draw.rect(self.disp, self.blackSquare, r, 0)
			if Position({'x': x, 'y': y}) in self.highlight:
				pygame.draw.rect(self.disp, self.highlSquare, r, 10)
			
			p = pos.p
			
			if p is not None:
				if p.team == "white":
					dy = 0
				else:
					dy = 1
				self.disp.blit(self.piecesImage[p.pieceId][dy], (x*80, y*80))

