#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Loop principale della GUI """

import pygame, sys
from pygame.locals import *

from pieces import *
import players, board

# Funzioni di debug
import pdb

# Definisce alcuni parametri da usare con pygame
MOUSEBUTTONLEFT = 1
MOUSEBUTTONRIGHT = 3

# Imposta il numero di aggiornamenti al secondo della finestra
MAXFPS = 30
fpsClock = pygame.time.Clock()

# Inizializza la finestra di pygame
pygame.init()
DISP = pygame.display.set_mode((640, 640), 0, 32)

# Imposta il titolo della finestra
pygame.display.set_caption('Scacchi')

# Mostra un testo sullo schermo
def showMessage(disp, message, frames = 300):
	font = pygame.font.Font(None, 36)
	
	disp.fill((0, 0, 0))
	
	lines = message.splitlines()
	texts = [font.render(line, 1, (255, 255, 255))  for line in lines]
	dy = max([text.get_height() for text in texts])
	
	x, y = disp.get_size()
	
	for i, text in enumerate(texts):
		dx = text.get_width()
		disp.blit(text, ((x - dx)/2, y/2 - dy * (len(texts) - i) ))
	
	for i in range(frames + 1):
		for event in pygame.event.get():
			if event.type == QUIT:
				pygame.quit()
				sys.exit()

		pygame.display.update()
		fpsClock.tick(MAXFPS)

# Prova a caricare le impostazioni personalizzate
try:
	from config import *
	
	for pl in ["white", "black"]:
		assert pl in CONFIGS
		assert "type" in CONFIGS[pl]
except Exception, e:
	print str(e)
	CONFIGS = players.DEFAULTS
	showMessage(DISP, "Impossibile caricare il file config.py\nSaranno caricati i valori predefiniti")

# Cancella il testo, se necessario
DISP.fill((0, 0, 0))

# Mostra un testo di caricamento
showMessage(DISP, "Caricamento...", 0)

try:
	# Dichiara una nuova partita
	game = players.Game(DISP, CONFIGS)
except ChessError, e:
	showMessage(DISP, str(e))
except Exception, e:
	showMessage(DISP, str(e))
	pygame.quit()
	sys.exit()

gameover = False

try:
	while not gameover:
		for event in pygame.event.get():
			if event.type == QUIT:
				pygame.quit()
				sys.exit()
		
			if event.type == MOUSEBUTTONDOWN:	# È stato cliccato un tasto del mouse
				if event.button == MOUSEBUTTONLEFT:	# Se è il tasto sinistro ...
					x, y = event.pos
					x = int(x/80)
					y = int(y/80)		# ... calcola quale casella è stata cliccata
					game.whoseTurn().userSelected(x, y)	# esegue la selezione
				if event.button == MOUSEBUTTONRIGHT:
					game.whoseTurn().userUnselected()	# se invece è il tasto destro esegue la deselezione
	
		m = game.playTurn()			# Acquisisce la mossa fatta dal giocatore
		if m is not None:				# Se non ha ancora deciso, allora m == None
			(pos, newpos) = m
			if game.board.isLegal(pos, newpos):	# Altrimenti verifica la validità della mossa
				game.board.move(pos, newpos)	# E la esegue
				game.history += [(pos, newpos)]
			
				# Verifico che il giocatore non sia in scacco matto
				player = game.whoseTurn()
			
				if game.board.isStuck(player.team): # player non ha più mosse disponibili
					gameover = True
				else:
					player.reportMove(pos, newpos)
				
				game.board.highlight = []	# Pronti per il prossimo turno!
			
		game.board.draw()	# Disegna la scacchiera e i pezzi in esso contenuti ...
		fpsClock.tick(MAXFPS)	# ... ma massimo MAXFPS volte al secondo
		pygame.display.update()
except Exception, e:
	showMessage(DISP, "Si è verificato un errore sconosciuto\n" + str(e))
	pygame.quit()
	sys.exit()
	
	
while True:
	# player è in scacco matto!
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()

	game.board.draw()
	player.lose(DISP)
	pygame.display.update()
	fpsClock.tick(MAXFPS)

