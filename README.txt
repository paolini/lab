Progetti del corso di
Laboratorio di software per le applicazioni
Universita' di Firenze
-------------------------------------------

1. surface:
Calcolo e visualizzazione di superfici di area minima
  
2. signal:
Analisi di un segnale audio e riconoscimento armoniche

3. rsa:
Implementazione dell'algoritmo RSA di crittografia a chiave pubblica

4. chess:
il giuoco degli scacchi

5. 2048:
puzzle game

6. mines:
solitario (minesweeper)

7. maze:
gioco labirinto

8. trains:
???
