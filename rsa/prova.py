p=37
q=31
n=p*q
F=(p-1)*(q-1)
e=7 #numero minore di F e coprimo con F
d=463  #inverso moltiplicativo di e modulo F
CPRIV=tuple([e,n])
CPUBBL=tuple([d,n])

print "La chiave pubblica e' ", CPUBBL

testo = raw_input('Inserire il testo da crittografare : ')

L = list(testo) #testo in input lo metto in una lista separando ogni carattere (spazi e punteggiature comprese)
#print L
A = []
L1 = [ord(j) for j in L] #converto ogni elemento della lista nel corrispondente valore ascii (che e' un intero)
#print L1
L = [int((j**e)%n) for j in L1] #codifico ogni intero con la chiave pubblica generata inizialmente
#print L
for i in range(0,len(L),4):  #dividiamo il testo codificato in stringhe da 4 componenti ciascuna
	li=tuple(L[i:i+4])
	#print li
	A.append(li)

print A #lista di tuple ognuna delle quali ha 4 componenti convertite in ascii

#scegliendo p=37 e q=31
#m in codifica ascii e' 109 , facendo la cifratura a mano m corrisponde a 1093
#1093=(109**7)%1147
#dunque la codifica  dovrebbe funzionare.

#se fatta a mano su una singola lettera la decodifica funziona!



"""
p=37
q=31
N=p*q
F=(p-1)*(q-1)
e=7 #numero minore di F e coprimo con F
d=463  #inverso moltiplicativo di e modulo F
CPRIV=tuple([e,N])
CPUBBL=tuple([d,N])

print "La chiave pubblica e' ", CPUBBL
"""