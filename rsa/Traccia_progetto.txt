Ad ogni lettera dell'alfabeto e eventualmente ad ogni numero e ad ogni simbolo di punteggiatura associamo un numero (esempio A=1,   Z=26).
3 parti: generazione chiavi
	 cifratura messaggio (si prova a fare il messaggio composto da più di una lettera)
	 decifratura messaggio

Nella generazione delle chiavi andranno scelti due primi p e q (non troppo vicini e nemmeno troppo lontani tra loro)
n=p*q e f(n)=(p-1)(q-1)
scelta di chiave pubblica e=primo con f(n) e minore di f(n) (possibilmente fare una lista di quelli possibili e scegliere uno tra di essi casualmente evitando di scegliere p o q).
scelta chiave privata d=è tale che il suo prodotto con e sia congruo ad 1 modulo f(n).

Nella cifratura del messaggio viene selezionato il carattere da cifrare che viene letto poi come il numero che gli era stato precedentemente dato. Scelto il carattere viene cifrato come (messaggio)^e modulo n.

Nella decifratura abbiamo a disposizione messaggio cifrato, d e n e per decifrare basta l'operazione (m_cifrato)^d modulo n. Il risultato sarà un numero che corrisponde a una lettera, simbolo o numero. 

Il messaggio da decifrare viene inserito su terminale e si stampa la sua cifratura.


- Nel caso il testo sia stato scritto in minuscolo uso comando upper per trasformare testo con lettere maiuscole a cui applicare poi la cifratura
- Deve essere reso pubblico n (che in teoria è difficile da fattorizzare) e la chiave pubblica e


p==37
q==31
N
