# Giulia Marini, Valentina Scappini, Alessia Innocenti, Giacomo Giovanni Pizzuti

# -*- coding: utf8 -*-

def mcd(a, b):
    """Restituisce il Massimo Comune Divisore tra a e b"""
    while b:
        a, b = b, a%b
    return a

#genera la chiave pubblica e la chiave privata per la codifica e decodifica del testo in input
#la scelta dei due primi e' a caso (scelti tra 3 e 151 dalla funzione choice)
import random
s = file('numeriprimi.txt').read()
primi = s.split('\t')
p = random.choice(primi)
q = random.choice(primi)
p = int(p)
q = int(q)
n = p*q #modulo corrispondente alla chiave pubblica e privata
F = (p-1)*(q-1) #phi di eulero

#genero una lista con i coprimi di F da cui scelgo a caso l'esponente pubblico
l = []
for e in range(2,F-1):
	if mcd(e,F) == 1:
		l.append(e)

e = random.choice(l)

#genero una lista con i numeri che soddisfano la congruenza e*d = 1 mod(F) da cui scelgo a caso l'esponente privato
ll = []
for d in range(2,n):
	if (e*d)%F == 1:
		ll.append(d)

d = random.choice(ll)

CPUBBL = tuple([e,n])
CPRIV = tuple([d,n])
print " La chiave pubblica: ", CPUBBL
#print " La chiave privata: ", CPRIV

#richiesta all'utente di inserire il testo da cifrare
testo = raw_input('Inserire il testo da crittografare : ')

#testo in input lo metto in una lista separando ogni carattere (spazi e punteggiature comprese)
L = list(testo)

#converto ogni elemento della lista nel corrispondente valore ascii (che e' un intero) con la funzione predefinita ord
L = [ord(j) for j in L]

#codifico ogni intero con la chiave pubblica generata inizialmente
L = [int((j**e)%n) for j in L]

#aggiungo al testo cifrato l'esponente privato in modo che chi riceve il messaggio ha modo di decifrarlo
L.append(d)



print "Il testo cifrato e' : ", L

