# Giulia Marini, Valentina Scappini, Alessia Innocenti, Giacomo Giovanni Pizzuti

# -*- coding: utf8 -*-

#richiesta all'utente di inserire il modulo della chiave
n = raw_input('Inserire n: ')

#richiesta all'utente di inserire gli interi che compongono il testo da decodificare
testo = raw_input('Inserire il testo cifrato: ')

#elimino parentesi quadre e tonde all'inizio e alla fine del testo in input
testo = testo[1:len(testo)-1]

#elimino virgole del testo in input e metto in una lista gli interi(con parentesi e spazi )
t = testo.split(',')

#estraggo l'esponente privato dal messaggio inserito
a = len(t)
d = t[a-1]

#tolgo l'esponente privato prima di decodificare il messaggio inserito
t = t[:a-1]

#decodifico gli elementi della lista con la chiave privata
t = [(int(i)**int(d))%int(n) for i in t]

#converto gli interi dal codice ascii in stringa
t = ''.join(chr(i%256) for i in t) #chr e' la funzione inversa di ord

print "Il testo decodificato e' :", t
