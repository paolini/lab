import sys, pygame, time
from background import Background
from tessera import Tessera
import random

class Game(object):

    def __init__(self):
        self.size = (280, 280)
        self.background = Background(self.size)
        self.tessere = []
        self.schermata_iniziale()

        pygame.init()

        self.screen = pygame.display.set_mode(self.size)

    def muovi_sud(self):
        d = {}
        for tessera in self.tessere:
            d[tessera.pos] = tessera

        mosso = True
        while mosso:
            mosso = False
            for tessera in self.tessere:
                new_pos = (tessera.pos[0],tessera.pos[1]+1)
                if new_pos[1] <= 3 and new_pos not in d:
                    del d[tessera.pos]
                    tessera.pos = new_pos
                    d[new_pos] = tessera
                    mosso = True

        occ = []
        for tessera in self.tessere:
            occ.append(tessera.pos)
        for x in range(4):
            if (x,3) in occ and (x,2) in occ:
                      if d[(x,3)].n == d[(x,2)].n :
                          self.tessere.remove(d[(x,3)])
                          self.tessere.remove(d[(x,2)])
                          self.tessere.append(Tessera(d[(x,3)].n*2,x,3))

                          if (x,1) in occ and (x,0) in occ:
                                if d[(x,1)].n == d[(x,0)].n :
                                    self.tessere.remove(d[(x,1)])
                                    self.tessere.remove(d[(x,0)])
                                    self.tessere.append(Tessera(d[(x,1)].n*2,x,1))
                      elif (x,2) in occ and (x,1) in occ:
                           if d[(x,2)].n == d[(x,1)].n :
                              self.tessere.remove(d[(x,2)])
                              self.tessere.remove(d[(x,1)])
                              self.tessere.append(Tessera(d[(x,2)].n*2,x,2))
                           elif (x,1) in occ and (x,0) in occ:
                                if d[(x,1)].n == d[(x,0)].n :
                                    self.tessere.remove(d[(x,1)])
                                    self.tessere.remove(d[(x,0)])
                                    self.tessere.append(Tessera(d[(x,1)].n*2,x,1))



    def muovi_nord(self):
        d = {}
        for tessera in self.tessere:
            d[tessera.pos] = tessera

        mosso = True
        while mosso:
            mosso = False
            for tessera in self.tessere:
                new_pos = (tessera.pos[0],tessera.pos[1]-1)
                if new_pos[1] >= 0 and new_pos not in d:
                    del d[tessera.pos]
                    tessera.pos = new_pos
                    d[new_pos] = tessera
                    mosso = True

        occ=[]
        for tessera in self.tessere:
            occ.append(tessera.pos)
        for x in range(4):
            if (x,0) in occ and (x,1) in occ:
                      if d[(x,0)].n == d[(x,1)].n :
                          self.tessere.remove(d[(x,0)])
                          self.tessere.remove(d[(x,1)])
                          self.tessere.append(Tessera(d[(x,0)].n*2,x,0))
                          if (x,2) in occ and (x,3) in occ:
                                if d[(x,2)].n == d[(x,3)].n :
                                    self.tessere.remove(d[(x,2)])
                                    self.tessere.remove(d[(x,3)])
                                    self.tessere.append(Tessera(d[(x,2)].n*2,x,2))
                      elif (x,1) in occ and (x,2) in occ:
                           if d[(x,1)].n == d[(x,2)].n :
                              self.tessere.remove(d[(x,2)])
                              self.tessere.remove(d[(x,1)])
                              self.tessere.append(Tessera(d[(x,1)].n*2,x,1))
                           elif (x,2) in occ and (x,3) in occ:
                                if d[(x,2)].n == d[(x,3)].n :
                                    self.tessere.remove(d[(x,2)])
                                    self.tessere.remove(d[(x,3)])
                                    self.tessere.append(Tessera(d[(x,2)].n*2,x,2))


    def muovi_ovest(self):
        d = {}
        for tessera in self.tessere:
            d[tessera.pos] = tessera

        mosso = True
        while mosso:
            mosso = False
            for tessera in self.tessere:
                new_pos = (tessera.pos[0]-1,tessera.pos[1])
                if new_pos[0] >= 0 and new_pos not in d:
                    del d[tessera.pos]
                    tessera.pos = new_pos
                    d[new_pos] = tessera
                    mosso = True

        occ=[]
        for tessera in self.tessere:
            occ.append(tessera.pos)
        for y in range(4):
            if (0,y) in occ and (1,y) in occ:
                      if d[(0,y)].n == d[(1,y)].n :
                          self.tessere.remove(d[(0,y)])
                          self.tessere.remove(d[(1,y)])
                          self.tessere.append(Tessera(d[(0,y)].n*2,0,y))
                          if (2,y) in occ and (3,y) in occ:
                                if d[(2,y)].n == d[(3,y)].n :
                                    self.tessere.remove(d[(2,y)])
                                    self.tessere.remove(d[(3,y)])
                                    self.tessere.append(Tessera(d[(2,y)].n*2,2,y))
                      elif (1,y) in occ and (2,y) in occ:
                           if d[(1,y)].n == d[(2,y)].n :
                              self.tessere.remove(d[(2,y)])
                              self.tessere.remove(d[(1,y)])
                              self.tessere.append(Tessera(d[(1,y)].n*2,1,y))
                           elif (2,y) in occ and (3,y) in occ:
                                if d[(2,y)].n == d[(3,y)].n :
                                    self.tessere.remove(d[(2,y)])
                                    self.tessere.remove(d[(3,y)])
                                    self.tessere.append(Tessera(d[(2,y)].n*2,2,y))


    def muovi_est(self):
        d = {}
        for tessera in self.tessere:
            d[tessera.pos] = tessera

        mosso = True
        while mosso:
            mosso = False
            for tessera in self.tessere:
                new_pos = (tessera.pos[0]+1,tessera.pos[1])
                if new_pos[0] <= 3 and new_pos not in d:
                    del d[tessera.pos]
                    tessera.pos = new_pos
                    d[new_pos] = tessera
                    mosso = True

        occ=[]
        for tessera in self.tessere:
            occ.append(tessera.pos)
        for y in range(4):
            if (3,y) in occ and (2,y) in occ:
                      if d[(3,y)].n == d[(2,y)].n :
                          self.tessere.remove(d[(3,y)])
                          self.tessere.remove(d[(2,y)])
                          self.tessere.append(Tessera(d[(3,y)].n*2,3,y))
                          if (1,y) in occ and (0,y) in occ:
                                if d[(1,y)].n == d[(0,y)].n :
                                    self.tessere.remove(d[(1,y)])
                                    self.tessere.remove(d[(0,y)])
                                    self.tessere.append(Tessera(d[(1,y)].n*2,1,y))
                      elif (2,y) in occ and (1,y) in occ:
                           if d[(2,y)].n == d[(1,y)].n :
                              self.tessere.remove(d[(2,y)])
                              self.tessere.remove(d[(1,y)])
                              self.tessere.append(Tessera(d[(2,y)].n*2,2,y))
                           elif (1,y) in occ and (0,y) in occ:
                                if d[(1,y)].n == d[(0,y)].n :
                                    self.tessere.remove(d[(1,y)])
                                    self.tessere.remove(d[(0,y)])
                                    self.tessere.append(Tessera(d[(1,y)].n*2,1,y))




    def schermata_iniziale(self):
        self.aggiungi_tessera()
        self.aggiungi_tessera()

    def aggiungi_tessera(self):
        l = [(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1), (2, 2), (2, 3), (3, 0), (3, 1), (3, 2), (3, 3)]
        for tessera in self.tessere:
            l.remove(tessera.pos)




        X = random.choice(l)
        x = X[0]
        y = X[1]
        i = random.choice([2, 2, 2, 2, 4])
        self.tessere.append(Tessera(i,x,y))

    def run(self):
            while True:
                 for event in pygame.event.get():
                     if event.type == pygame.QUIT:
                        return
                     elif event.type == pygame.KEYDOWN:
                       if event.key == pygame.K_ESCAPE:
                          return
                       elif event.key == pygame.K_DOWN:
                           self.muovi_sud()
                           self.aggiungi_tessera()
                       elif event.key == pygame.K_UP:
                           self.muovi_nord()
                           self.aggiungi_tessera()
                       elif event.key == pygame.K_LEFT:
                           self.muovi_ovest()
                           self.aggiungi_tessera()
                       elif event.key == pygame.K_RIGHT:
                           self.muovi_est()
                           self.aggiungi_tessera()


                 for t in self.tessere:
                     t.update(self.screen)

                 self.background.draw(self.screen)
                 for t in self.tessere:
                      t.draw(self.screen)

                 time.sleep(0.01)

                 pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run()