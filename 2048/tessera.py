import pygame
from pygame import Rect

width = 70
height = 70
images={}
for n in range(11):
    file_name="%s.png" %2**(n+1)
    img = pygame.image.load(file_name)
    img = pygame.transform.scale(img,(width,height))
    images[2**(n+1)]= img

class Tessera(object):
    def __init__(self,n,x,y):
        self.n = n
        self.img = images[n]
        self.pos = (x,y)

    def update(self,screen):
        pass

    def draw(self,screen):
        screen.blit(self.img,(self.pos[0]*width,self.pos[1]*height))