from pygame import Surface

class Background(Surface):
    color = (0,0,0)

    def __init__(self,size):
        super(Background,self).__init__(size)
        self.fill(self.color)

    def draw(self,screen):
        screen.blit(self,(0,0))
