from scipy.io import wavfile
from numpy.fft import fft
import matplotlib.pyplot as plt

filename='440Hz.wav'

fs, data = wavfile.read(filename)

print type(fs), type(data)
print fs, data.shape

freq = fft(data).real

plt.plot(freq[1000:1500])
plt.ylabel('fft')
plt.show()
