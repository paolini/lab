import sys, random, pygame


class Serpente(object):
    def __init__(self):
        k = random.choice(range(3, 18))
        s = random.choice(range(3, 18))

        testa = Segment(k, s)

        self.corpo = [testa]
        for i in [1, 2]:
            self.corpo.append(Segment(k + i, s))

        self.direzione = 0

    def draw(self,screen):
        for segment in self.corpo:
            segment.draw(screen)

    def update(self):
        """muove serpente"""
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    self.direzione = 0
                elif event.key == pygame.K_UP:
                    self.direzione = 1
                elif event.key == pygame.K_LEFT:
                    self.direzione = 2
                elif event.key == pygame.K_DOWN:
                    self.direzione = 3

    #algoritmo per lo spostamento del serpente
        i = len(self.corpo) - 1
        while i > 0:
            self.corpo[i].k = self.corpo[i - 1].k   #il blocco del corpo prende le coordinate del blocco precedente
            self.corpo[i].s = self.corpo[i - 1].s
            i -= 1  #decremento l'indice per controllare un altro blocco

    #muovo il serpente in base al tasto che ho premuto

        if self.direzione == 0:
            self.corpo[0].k += 1
            spostamento_effettuato = '1'
        elif self.direzione == 2:
            self.corpo[0].k -= 1
            spostamento_effettuato = '1'
        elif self.direzione == 3:
            self.corpo[0].s += 1
            spostamento_effettuato = '1'
        elif self.direzione == 1:
            self.corpo[0].s -= 1
            spostamento_effettuato = '1'

class Segment(pygame.sprite.Sprite):
    def __init__(self, k, s):

        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.Surface([15, 15])
        VERDE = (5, 245, 5)
        self.image.fill(VERDE)

        self.rect = self.image.get_rect()
        self.k = k
        self.s = s

    def draw(self, screen):
        """disegna il segmento sullo schermo"""
        screen.blit(self.image,(self.k*15,self.s*15))






