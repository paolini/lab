imm_sfondo = "immagine_snake.jpg"
import pygame, time
from pygame.locals import *
from sys import exit
from serpente import Serpente
from sfondo import Sfondo
pygame.init()

screen = pygame.display.set_mode((640,480), DOUBLEBUF | HWSURFACE, 32)
pygame.display.set_caption("gioca!")
speed = 0.5
orologio = pygame.time.Clock()


#sfondo = pygame.image.load(imm_sfondo).convert()
sfondo = Sfondo((300,300))
serpente = Serpente()

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            exit()
    sfondo.draw(screen)
    serpente.draw(screen)
    pygame.display.flip()
    time.sleep(1.0)

    serpente.update()
