from pygame import Surface

class Sfondo(Surface):
    color = (250,0,0)

    def __init__(self,size):
        super(Sfondo,self).__init__(size)
        self.fill(self.color)

    def draw(self,screen):
        screen.blit(self,(0,0))