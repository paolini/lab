import random
from cell import Cell

class Grid(object):


    def __init__(self, m, n,number_of_mines):
        self.grid = [[Cell(i, j) for j in range(n)] for i in range(m)]

        empty_cells = []
        for row in self.grid:
            for cell in row:
                empty_cells.append(cell)
        while number_of_mines > 0:
            for a in range(number_of_mines):
                mine = random.randrange(len(empty_cells))
                pos_row = mine//n
                pos_col = (mine % n)-1
                if not self.grid[pos_row][pos_col].has_mine:
                    self.placed_mine(pos_row, pos_col)
                    number_of_mines = number_of_mines-1
        for i in range(m):
            for j in range(n):
                if self.grid[i][j].has_mine:
                    for ii in range( i-1, i+2):
                        for jj in range( j-1, j+2):
                            if ii==-1 or jj== -1 or ii==m or jj==n:
                                continue
                            self.grid[ii][jj].number += 1

    def draw(self, screen):
        for row in self.grid:
            for cell in row:
                cell.draw(screen)

    def pressed(self, i, j):
        return self.grid[i][j].pressed()

    def placed_flag(self, i, j):
        self.grid[i][j].placed_flag()

    def placed_mine(self, i, j):
        self.grid[i][j].placed_mine()

    def placed_number(self, i, j):
        self.grid[i][j].placed_number()

    def show(self):
        for row in self.grid:
            for cell in row:
                cell.visited = True
