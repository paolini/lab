import sys, pygame, time
from background import Background
from cell import Cell

class Game(object):

    def __init__(self):
        self.size = (220, 220)
        self.background = Background(self.size)
        self.cell = Cell()

        pygame.init()

        self.screen = pygame.display.set_mode(self.size)

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT: sys.exit()

            self.background.draw(self.screen)
            self.cell.draw(self.screen)

            time.sleep(0.01)

            pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run()