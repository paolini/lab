import sys, pygame, time
from background import Background
from grid import Grid
from cell import Cell


class Game(object):

    def __init__(self):
        pygame.init()
        self.reset((10,10),10)
        self.screen = pygame.display.set_mode(self.size)
        self.smile = None
        self.wait_for_smile = 0
        self.perso = pygame.image.load("perso.png")
        
    def reset(self,grid_size,number_of_mines):
        print 'reset', grid_size, number_of_mines
        self.size = (grid_size[0]*22,grid_size[1]*22+50)
        self.background = Background(self.size)
        self.grid = Grid(grid_size[0],grid_size[1],number_of_mines)
        self.screen = pygame.display.set_mode(self.size)


    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    x = (event.pos[1]-50)//22
                    y = event.pos[0]//22
                    if x < 1//22:
                        if y > 0 and y < 80//22:
                            self.reset((10,10),10)
                        if y > 80//22 and y < 160//22:
                            self.reset((15,15),40)
                        if y > 160//22 and y < 240//22:
                            self.reset((25,25),80)
                    else:
                        if self.grid.pressed(x, y):
                            self.grid.show()
                            self.smile = self.perso

                elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
                    x = (event.pos[1]-50)//22
                    y = event.pos[0]//22
                    if x >= 1//22:
                        self.grid.placed_flag(x, y)

            self.background.draw(self.screen)
            self.grid.draw(self.screen)

            if self.smile:
                self.screen.blit(self.smile, (self.size[0]/2, self.size[1]/2))

            time.sleep(0.01)

            pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run()