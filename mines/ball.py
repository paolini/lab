import pygame
from pygame import Rect

class Ball(Rect):
    def __init__(self):
        self.img = pygame.image.load("ball.png")
        super(Ball,self).__init__(self.img.get_rect())
        self.speed = [2, 2]

    def update(self, screen):
        self.move_ip(self.speed)
        if self.left < 0 or self.right > screen.get_width():
            self.speed[0] = -self.speed[0]
        if self.top < 0 or self.bottom > screen.get_height():
            self.speed[1] = -self.speed[1]

    def draw(self,screen):
        screen.blit(self.img,self) 