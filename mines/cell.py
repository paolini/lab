import pygame

class Cell(object):
    def __init__(self,i,j):
        self.i=i
        self.j=j
        self.has_mine = False
        self.has_flag = False
        self.visited = False
        self.number = 0
        self.cell = pygame.image.load("cell.png")
        self.flag = pygame.image.load("bandiera20x20.png")
        self.mine = pygame.image.load("mina20x20.png")
        self.numbers = [pygame.image.load("%s.png" %n) for n in range(9)]
        self.facile = pygame.image.load("facile.png")
        self.medio = pygame.image.load("medio.png")
        self.difficile = pygame.image.load("difficile.png")


    def draw(self,screen):
        if self.has_flag:
            img = self.flag
        elif self.visited and not self.has_mine:
            img = self.numbers[self.number]
        elif self.has_mine and self.visited:
            img = self.mine
        else:
            img = self.cell
        screen.blit(img, (self.j*22, self.i*22+50))
        screen.blit(self.facile, (0, 0))
        screen.blit(self.medio, (80, 0))
        screen.blit(self.difficile, (160, 0))

    def pressed(self):
        if self.has_flag:
            return False
        self.visited = True
        if self.has_mine:
            return True
        return False

    def placed_flag(self):
        if not self.visited:
            self.has_flag = True

    def placed_mine(self):
        if not self.visited:
            self.has_mine = True

    def placed_number(self):
        if not self.visited:
            self.has_number= True