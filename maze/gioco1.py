__author__ = 'm11gabghi'


import sys, pygame, time
from background import Background
from omino import omino
from vittoria import vittoria
class Game(object):
    def __init__(self):
        self.size = (1000, 500)
        self.background = Background(self.size)
        self.omino = omino([0,20])
        self.vittoria=vittoria()
        #self.mazes=mazes
        pygame.init()


        self.screen = pygame.display.set_mode(self.size)

        if self.background.matrice[25,0]==0:
            self.omino.pos=[0,25]
        else  : self.omino.pos=[0,26]

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
                    if self.background.matrice[self.omino.pos[1]+1,self.omino.pos[0]]==0:
                        self.omino.pos[1]=self.omino.pos[1]+1
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
                    if self.background.matrice[self.omino.pos[1]-1,self.omino.pos[0]]==0:
                        self.omino.pos[1]=self.omino.pos[1]-1
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                    if self.background.matrice[self.omino.pos[1],self.omino.pos[0]-1]==0:
                        self.omino.pos[0]=self.omino.pos[0]-1
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                    if self.background.matrice[self.omino.pos[1],self.omino.pos[0]+1]==0:
                        self.omino.pos[0]=self.omino.pos[0]+1
            #self.omino.update(self.screen)
            if self.omino.pos[0]== 80 :
               self.vittoria.draw(self.screen)
            else :
                self.background.draw(self.screen)
                self.omino.draw(self.screen)

            time.sleep(0.01)

            pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run()

