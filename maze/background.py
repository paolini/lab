from pygame import Rect
from pygame import image
from matrici import mazes
import matplotlib.pyplot as pyplot
#class omino(Rect):
#    def __init__(self):
#       self.img = pygame.image.load("omino.png")
#       super(omino,self).__init__(self.img.get_rect())
class Background(Rect):
    color = (255,255,255)

    #def __init__(self,size):
    #    super(Background,self).__init__(size)
    #    self.fill(self.color)
    #    self.matrice = mazes()



    def __init__(self,size):
        self.matrice=mazes(80,50)
        pyplot.figure(figsize=(10, 5))
        pyplot.imshow(self.matrice, cmap=pyplot.cm.binary, interpolation='nearest')
        pyplot.xticks([]), pyplot.yticks([])
        pyplot.savefig("labirinto.png")
        self.img = image.load("labirinto.png")
        super(Background, self).__init__(self.img.get_rect())

    def draw(self,screen):
        screen.blit(self.img,self)

    #def draw(self,screen):
    #    screen.blit(self,(0,0))

