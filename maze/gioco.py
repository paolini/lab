import sys, pygame, time
from background import Background
from omino import omino
from matrici import mazes

class Game(object):
    def __init__(self):
        self.size = (500, 500)
        self.background = Background(self.size)
        self.omino = omino()
        self.posizione=(0,0)

        pygame.init()


        self.screen = pygame.display.set_mode(self.size)

        if mazes[25 , 0]==0:
            self.posizione=(25,0)
        else  : self.posizione=(26,0)

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
                    if mazes(self.posizione[0]+1,self.posizione[1])==0:
                        self.posizione[0]=self.posizione[0]+1
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
                    if mazes(self.posizione[0]-1,self.posizione[1])==0:
                        self.posizione[0]=self.posizione[0]-1
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                    if mazes(self.posizione[0],self.posizione[1]-1)==0:
                        self.posizione[1]=self.posizione[1]-1
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                    if mazes(self.posizione[0],self.posizione[1]+1)==0:
                        self.posizione[1]=self.posizione[1]+1
            self.omino.update(self.screen)

            self.background.draw(self.screen)
            self.omino.draw(self.screen)

            time.sleep(0.01)

            pygame.display.flip()

if __name__ == '__main__':
    game = Game()
    game.run()

